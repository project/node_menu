<?php

/**
 * Implementation of hook_views_tables().
 */
function node_menu_views_tables() {
  $tables['node_menu'] = array(
    'name' => 'node_menu',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid',
      ),
      'right' => array(
        'field' => 'nm_node_id',
      ),
    ),
  );
  $tables['menu'] = array(
    'name' => 'menu',
    'join' => array(
      'left' => array(
        'table' => 'node_menu',
        'field' => 'nm_menu_id',
      ),
      'right' => array(
        'field' => 'mid',
      ),
    ),
    'fields' => array(
      'path' => array(
        'name' => t('Node menu: Node title + Menu link'),
        'handler' => array(
          'node_menu_handler_field_link' => t('Normal'),
        ),
        'option' => array(
           '#type' => 'select',
           '#options' => array(
             'link' => 'As link',
             'nolink' => 'Without link'
            ),
        ),
        'sortable' => true,
        'help' => t('Display node title linked to its menu url.'),
      ),
    ),
    'sorts' => array(
      'weight' => array(
        'name' => t('Nome menu: Item weight'),
      ),
    ),
    'filters' => module_exists('menu_per_role') ? array(
      'mid' => array(
        'name' => t('Node menu: User role'),
        'operator' => array('=' => t('Has access')),
        'list' => 'views_handler_operator_yesno',
        'list-type' => 'select',
        'handler' => 'node_menu_handler_filter_role_custom',
        'help' => t('Include the node only if user is a member of roles with access to menu item.'),
      ),
    ) : NULL,
  );
  
  return $tables;
}

/**
 * Filter handler for menu ID, retricts items according to menu_per_role permissions.
 */
function node_menu_handler_filter_role_custom($op, $filter, $filterinfo, &$query) {
  global $user;
  
  // Traverse role IDs
  $roles = array_keys($user->roles);
  
  // Validation with sub-queries
  $where = "
menu.mid NOT IN(SELECT mid FROM {menu_per_role} GROUP BY mid) OR
menu.mid IN(
  SELECT mid
  FROM {menu_per_role} mpr
  WHERE
    mpr.mid = menu.mid AND /* Restrict to parent menu table */
    mpr.rid IN ('". implode("','", $roles) ."')
  GROUP BY mid
)";
  $query->add_where($where);
}

/**
 * Field handler for node menu title.
 */
function node_menu_handler_field_link($fieldinfo, $fielddata, $value, $data) {
  $args = func_get_args();
  
  if ($fielddata['options'] == 'nolink') {
    return check_plain($data->node_title);
  }
  
  $query = NULL;
  if ($value) {
    $parsed_url = parse_url($value);
    if ($parsed_url['query'] && !$parsed_url['host']) {
      $path = $parsed_url['path'];
      $query = $parsed_url['query'];
    }
    else {
      $path = $value;
    }
  }
  else {
    $path = "node/$data->nid";
  }
  
  return l($data->node_title, $path, NULL, $query);
}
