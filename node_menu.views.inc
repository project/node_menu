<?php

/**
 * Implementation of hook_views_data().
 */
function node_menu_views_data() {
  // ----------------------------------------------------------------
  // node_menu table

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['node_menu']['table']['group']  = t('Node menu');
/*

  $data['node_menu']['table']['base'] = array(
    'field' => 'nm_id',
    'title' => t('Node Menu'),
    'help' => t('Relationships between nodes and menus.'),
  );
*/

  $data['node_menu']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nm_node_id',
      'type' => 'INNER',
    ),
  );

  $data['node_menu']['nm_menu_id'] = array(
    'title' => t('Menu link ID'),
    'help' => t('Unique menu link identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'base' => 'menu_links',
      'field' => 'mlid',
      'handler' => 'views_handler_relationship',
      'label' => t('Menu links'),
    ),
  );

  // ----------------------------------------------------------------
  // node_menu table

  $data['menu_links']['table']['group'] = t('Node menu');

  $data['menu_links']['table']['base'] = array(
    'field' => 'mlid',
    'title' => t('Menu link ID'),
    'help' => t('Menu links created in your system.'),
  );

  $data['menu_links']['table']['join'] = array(
    'node_menu' => array(
      'left_field' => 'nm_menu_id',
      'field' => 'mlid',
      'type' => 'INNER',
    ),
  );

  $data['menu_links']['mlid'] = array(
    'title' => t('Link title'),
    'help' => t('Display node title linked to its node menu url.'),
    'field' => array(
      'handler' => 'node_menu_handler_field_link_title',
    ),
  );

  $data['menu_links']['link_path'] = array(
    'title' => t('Link'),
    'help' => t('Display link with custom text.'),
    'field' => array(
      'handler' => 'node_menu_handler_field_link',
    ),
  );

  return $data;
}

/**
 * Field handler for node menu title.
 */
class node_menu_handler_field_link_title extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_path'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_path'] = array(
      '#title' => t('Link this field to its path'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_path']),
    );
  }

  /**
   * Render whatever the data is as a link to the path.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    node_menu_nodeapi($values, 'prepare');
    if (!empty($this->options['link_to_path'])) {
      return l($values->node_menu['title'], $values->node_menu['link_path'], array('html' => TRUE));
    }
    else {
      return $values->node_menu['title'];
    }
  }

  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }
}

/**
 * Field handler for node menu link.
 */
class node_menu_handler_field_link extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['link_text'] = array('default' => t('read more'));
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_text'] = array(
      '#title' => t('Custom link text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['link_text'],
    );
  }
  
  /**
   * Render link to the path with a custom text.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    node_menu_nodeapi($values, 'prepare');
    return l($this->options['link_text'], $values->node_menu['link_path'], array('html' => TRUE));
  }

  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }
}
