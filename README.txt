
NODE MENU
Node as menu item and Field for Views
-------------------------------------
Core menu system let create menu items with a custom title and description.
Instead, this module uses node->title and node->body as corresponding title and
description. Path is node/#nid by default, you can specify another manually. This
module also provides a Views field so you can create a list of node menu items as
links addressing to given path instead of the node page.

6.x RELEASE
-----------
TODO
- Validate if given link path exists.

How to create a view
--------------------
1. Create a node view
2. Add a relationship using: "Node menu: Menu link ID". Be care to activate:
   "Require this relationship".
3. Add fields.
4. For menus with images, you can use Image module.

Blessings!
